# Introduction

[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)

Angular 2 Components Library:

- Gallery
- PageNav *(in dev)*


[**Live demo and descriptions**](https://alexrudol.com/projects/ar-ng2-components)


### > Prerequisites
- `node >= 6`
- `Angular >= 2.0.0`


