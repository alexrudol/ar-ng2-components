import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { NgGallerySlideshow } from './ng-gallery-slideshow';

@Component({
    selector: 'ng-gallery',
    encapsulation: ViewEncapsulation.None,
    styles: [ require('./ng-gallery.css').toString() ],
    templateUrl: 'ng-gallery.html'
})
export class NgGallery implements OnInit {

    // Gallery array
    @Input() gallery;

    // Parameters for the grid
    @Input() grid: string = 'stacked';
    @Input() effect: string = 'brightness';

    // Parameters for the slideshow
    @Input() pager: string;
    @Input() counter: string;
    @Input() fields: string[];


    ngOnInit() {

        // Check if all subarrays are included
        if (!this.gallery.images) { console.log('No images in the array! Please add images paths.') }
        else {
            if (!this.gallery.titles) { this.gallery.titles = []; }
            if (!this.gallery.thumbs) { this.gallery.thumbs = this.gallery.images; }
            if (!this.gallery.images_big) { this.gallery.images_big = this.gallery.images; }
            if (!this.gallery.authors) { this.gallery.authors = []; }
            if (!this.gallery.dates) { this.gallery.dates = []; }
            if (!this.gallery.descriptions) { this.gallery.descriptions = []; }
            if (!this.gallery.locations) { this.gallery.locations = []; }
            if (!this.gallery.maps) { this.gallery.maps = []; }
        }

    }

}
