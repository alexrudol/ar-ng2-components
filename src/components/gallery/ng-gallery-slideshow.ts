import { Component, ViewEncapsulation, Input, HostListener } from '@angular/core';


@Component({
    selector: 'gallery-slideshow',
    encapsulation: ViewEncapsulation.None,
    styles: [ require('./ng-gallery-slideshow.css').toString() ],
    templateUrl: 'ng-gallery-slideshow.html',
})
export class NgGallerySlideshow {

    // Gallery array
    @Input() gallery;

    // Parameters for the slideshow
    @Input() pager: string = 'line';
    @Input() counter: string = 'auto';
    @Input() fields: string[];
        // Fields
        field_title: boolean = false;
        field_author: boolean = false;
        field_date: boolean = false;
        field_location: boolean = false;
        field_map: boolean = false;
        field_description: boolean = false;

    // Conditions
    openImage: number;
    navShow: boolean = true;
    navDisabled: boolean = false;

    descriptionLongText: boolean = false;
    descriptionOpen: boolean = false;

    slideNext: boolean = false;
    slidePrev: boolean = false;
    slideNoAnim: boolean = false;


    slideshowOpen(slide) {
        this.openImage = slide;
        this.descriptionCheck();
        this.descriptionSetFields(this.fields);
    }


    // TOP AND SIDE NAVIGATION

    navToggle() {
        this.navShow = this.navShow != true;
    }

    slideshowClose() {
        this.openImage = null;
    }

    navPrev() {
        if (this.openImage+1 != 1 && !this.navDisabled) {
        this.navDisabled = true;
        this.slidePrev = true;
        this.descriptionOpen = false;
        setTimeout( () => {
            this.slideNoAnim = true;
            this.slidePrev = false;
            this.openImage = this.openImage - 1;
            this.descriptionCheck();
            setTimeout( ()=> {
                this.slideNoAnim = false;
                this.navDisabled = false;
            }, 10);
        }, 400);
        }
    }

    navNext() {
        if (this.openImage+1 != this.gallery.images.length && !this.navDisabled) {
        this.navDisabled = true;
        this.slideNext = true;
        this.descriptionOpen = false;
        setTimeout( () => {
            this.slideNoAnim = true;
            this.slideNext = false;
            this.openImage = this.openImage + 1;
            this.descriptionCheck();
            setTimeout( ()=> {
                this.slideNoAnim = false;
                this.navDisabled = false;
            }, 10);
        }, 400);
        }
    }

    @HostListener('window:keydown', ['$event'])
    onKey(event: any) {
        if (event.key == 'ArrowRight') this.navNext();
        if (event.key == 'ArrowLeft') this.navPrev();
        if (event.key == 'ArrowUp' || event.key == 'ArrowDown') this.descriptionOpenToggle();
        if (event.key == 'Escape') this.slideshowClose();
        if (event.key == ' ') this.navToggle();
    }


    // DESCRIPTION

    // Set Description Fields values
    descriptionSetFields(fields) {
        this.field_title = false;
        this.field_author = false;
        this.field_date = false;
        this.field_location = false;
        this.field_map = false;
        this.field_description = false;
        for (let f = 0; f < this.fields.length; f++) {
            if ( fields[f] == 'title' ) this.field_title = true;
            if ( fields[f] == 'author' ) this.field_author = true;
            if ( fields[f] == 'date' ) this.field_date = true;
            if ( fields[f] == 'location' ) this.field_location = true;
            if ( fields[f] == 'map' ) this.field_map = true;
            if ( fields[f] == 'description' ) this.field_description = true;
        }
    }

    // Is the description short or long? Show\hide the expand button.
    descriptionCheck() {
        if (this.field_description && this.gallery.descriptions[this.openImage] != null) {
            this.descriptionLongText = this.gallery.descriptions[this.openImage].length > 330;
        } else this.descriptionLongText = false;
    }

    // Toggle the size of the description
    descriptionOpenToggle() {
        this.descriptionOpen = this.descriptionOpen != true;
    }


}