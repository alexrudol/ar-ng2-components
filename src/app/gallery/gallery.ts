import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'gallery-demo',
    templateUrl: './gallery.html'
})
export class Gallery implements OnInit {

    // Tweak'n'code

    grids = [
        { value: 'simple', id: 0 },
        { value: 'stacked', id: 1 },
        { value: 'main-top', id: 2 },
    ];
    grid = { value: 'stacked', id: 1 };

    effects = [
        { value: 'none', id: 0 },
        { value: 'scale', id: 1 },
        { value: 'opacity-in', id: 2 },
        { value: 'opacity-out', id: 3 },
        { value: 'brightness', id: 4 },
        { value: 'grayscale', id: 5 },
    ];
    effect = { value: 'brightness', id: 4 };

    pagers = [
        { value: 'none', id: 0 },
        { value: 'auto', id: 1 },
        { value: 'dots', id: 2 },
        { value: 'dashes', id: 3 },
        { value: 'line', id: 4 },
    ];
    pager = { value: 'auto', id: 1 };

    counters = [
        { value: 'auto', id: 0 },
        { value: 'always', id: 1 },
    ];
    counter = { value: 'auto', id: 0 };

    fields = [
        { value: 'title', id: 0 },
        { value: 'author', id: 1 },
        { value: 'date', id: 2 },
        { value: 'location', id: 3 },
        { value: 'map', id: 4 },
        { value: 'description', id: 5 },
    ];
    field = [
        { value: 'title', id: 0 },
        { value: 'location', id: 3 },
        { value: 'map', id: 4 },
    ];
    readyFields = [];

    showCode: boolean = false;

    // Gallery Array
    gallery = {
        titles: ['','Mice','Birds','Cat\'s nose','Croco','Owl','Lions','Wolf','Rhino','Sea turtle','Penguin','Zebras','Chikens','Hippo'],
        thumbs: [],
        images: [],
        images_big: [],
        authors: ['','John Doe','John Doe','','','','John Doe'],
        dates: ['','','01.01.2012'],
        descriptions: [
            '',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie nisl at felis iaculis porta. Suspendisse at velit urna. Phasellus blandit malesuada libero, id convallis nisi elementum tempor. Maecenas efficitur orci risus, facilisis vulputate turpis volutpat vel. Mauris interdum nisl erat, at laoreet nibh rutrum id. ',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat sit amet tortor non auctor. Donec in vehicula augue. Sed ante dolor, tincidunt ultricies hendrerit sed, lacinia nec lacus. Sed at ante ac ex tincidunt malesuada. Suspendisse pellentesque turpis nec nunc vehicula mollis. Quisque lectus eros, fringilla id malesuada eget, pharetra nec nisi. Morbi imperdiet venenatis risus, vehicula vulputate dolor varius non. Vestibulum odio purus, luctus quis laoreet et, dignissim ullamcorper purus. Ut convallis fringilla ante ut eleifend. In varius gravida sem sit amet commodo. Nulla facilisi. Maecenas elementum massa a odio imperdiet varius. Donec lacinia velit at odio aliquet pulvinar. Nulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sed pulvinar ligula. Cras volutpat pharetra dui ut porta. Sed elementum ultricies elit, ut dignissim mi rhoncus vel. Aliquam erat volutpat. Fusce ante ante, blandit vitae auctor eget, dignissim vel magna. Quisque eget orci lacus. Donec scelerisque quis quam sit amet interdum. Mauris posuere dignissim ex sit amet placerat. Phasellus elementum dolor non nisi vehicula pharetra. Mauris dignissim suscipit dui, et rhoncus arcu sodales vitae. Aliquam non tortor laoreet, eleifend enim ac, fermentum erat. Curabitur facilisis ante a diam commodo, et tincidunt nisi rhoncus. Aliquam ornare et leo posuere interdum. Ut accumsan semper turpis rhoncus molestie. Etiam neque leo, pellentesque nec ligula quis, ultricies vestibulum mauris. Fusce maximus sem ac magna dapibus posuere. Maecenas arcu nisi, tempus eu porta non, aliquet vitae nisi. Nunc iaculis, lorem scelerisque tempor blandit, turpis est sodales est, in gravida magna tortor ut mi. Donec mattis vel libero a aliquet. Pellentesque in ante arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc in semper turpis, quis bibendum erat. In aliquet nisi a arcu blandit varius nec nec diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec mollis massa vel orci consectetur posuere. Etiam quis volutpat ex. Aenean arcu nibh, mattis sit amet gravida sit amet, sollicitudin eu metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras vel elit ut tellus tincidunt rhoncus. Nulla semper aliquet odio at aliquam. Cras velit lacus, fringilla at accumsan vitae, bibendum vel augue. Cras vel erat viverra, ornare ex placerat, efficitur orci. Donec tincidunt tincidunt felis non porttitor. Suspendisse at sapien non purus eleifend auctor. Ut sed justo sit amet libero lacinia viverra non nec augue. Nam dictum neque vel mi gravida, sit amet volutpat ante accumsan. Fusce maximus mi sed pulvinar imperdiet. Proin ac massa enim. Pellentesque eu nisi aliquam, luctus elit id, auctor elit. Donec id velit neque. Mauris eu euismod tortor. Vivamus elementum, urna at ultricies dapibus, nisl dui mollis elit, nec luctus magna tortor porta nulla. Aenean fringilla nisi id libero ullamcorper, eget posuere magna tempus. Morbi auctor luctus sem, sit amet accumsan mi convallis a. Etiam viverra gravida lectus. Sed iaculis ut orci at pulvinar. Aliquam mauris tortor, vestibulum sodales nunc sed, rhoncus rhoncus metus. Vivamus sit amet lectus porttitor turpis ultrices interdum. Aenean ac ultrices felis, ut ullamcorper libero.',
            '',
            '',
            '',
            '',
            'Maecenas efficitur orci risus, facilisis vulputate turpis volutpat vel. Mauris interdum nisl erat, at laoreet nibh rutrum id. Maecenas fringilla porttitor volutpat. Donec eu egestas lorem. Donec et ipsum ac justo lobortis bibendum. Cras eleifend nisi eu dapibus egestas.'
        ],
        locations: ['','','','','','','','','Africa','','Antarktida'],
        maps: ['','','','','','','','','8.719444,21.241046','','-69.339020,79.624912']
    };

    constructor() {

        // Generate thumb paths
        for (let i = 1; i <= 14; i++) {
            this.gallery.thumbs.push('assets/img/thumb_image_'+i+'.jpg');
        }

        // Generate image paths
        for (let i = 1; i <= 14; i++) {
            this.gallery.images.push('assets/img/image_'+i+'.jpg');
        }

        // Generate big image paths
        for (let i = 1; i <= 14; i++) {
            this.gallery.images_big.push('assets/img/big_image_'+i+'.jpg');
        }
    }

    ngOnInit() {
        this.getFields();
    }

    // Generate fields array from options
    getFields() {
        this.readyFields = [];
        for (let f = 0; f < this.field.length; f++) {
            this.readyFields.push(this.field[f].value);
        }
    }

}
