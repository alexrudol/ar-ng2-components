import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Angular Material 2
import {
    MdButtonModule,
    MdIconModule,
    MdTabsModule,
    MdToolbarModule,
    MdCardModule,
} from "@angular/material/";
import { Ng2SelectModule } from 'ng2-material-select';

import { AppComponent } from "./app";
import { routes } from "./app.routes";
import { Toolbar } from './navigation/toolbar';

import { Home } from './home/home';
import { Gallery } from './gallery/gallery';
import { PageNav } from './pagenav/pagenav';

import { NgGallery } from '../components/gallery/ng-gallery';
import { NgGallerySlideshow } from "../components/gallery/ng-gallery-slideshow";


@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes, { useHash: false }),
        FormsModule,
        ReactiveFormsModule,

        // Angular Material 2
        MdButtonModule.forRoot(),
        MdIconModule.forRoot(),
        MdTabsModule.forRoot(),
        MdToolbarModule.forRoot(),
        MdCardModule.forRoot(),
        Ng2SelectModule,
    ],
    declarations: [
        AppComponent,
        Toolbar,
        Home,
        Gallery,
        PageNav,

        NgGallery,
        NgGallerySlideshow,
    ],
    providers: [
        { provide: LocationStrategy, useClass: PathLocationStrategy },
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
    constructor() {}
}
