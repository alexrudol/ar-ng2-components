import { Component } from '@angular/core';

@Component({
    selector: 'home',
    templateUrl: 'home.html',
    styles: [
        'md-card { display: inline-block; margin: 0 1% 1%; width: 47%; min-width: 300px; }' +
        'md-card:hover { cursor: pointer; }'
    ]
})
export class Home {

}
