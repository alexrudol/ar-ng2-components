import { Component } from '@angular/core';

@Component({
    selector: 'pagenav',
    template: '<h1>Page content navigation</h1><p>Auto generated side navigation for page content.</p><p><i>In development</i></p>',
})
export class PageNav {

}
