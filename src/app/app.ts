import { Component, ViewEncapsulation } from '@angular/core';
import { Toolbar } from './navigation/toolbar';

@Component({
    selector: 'app',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./app.scss'],
    templateUrl: './app.html'
})

export class AppComponent {

}
