import { Routes } from '@angular/router';

import { Home } from './home/home';
import { Gallery } from './gallery/gallery';
import { PageNav } from './pagenav/pagenav';

export const routes: Routes = [
    { path: '', component: Home },
    { path: 'gallery', component: Gallery },
    { path: 'pagenav', component: PageNav },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];
